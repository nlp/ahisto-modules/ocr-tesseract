FROM ubuntu:latest

ENV DEBIAN_FRONTEND=noninteractive \
    TERM=xterm

RUN set -e -o xtrace \
  ; apt-get -qy update \
  ; apt-get -qy upgrade \
  ; apt-get -qy install --no-install-recommends ca-certificates \
                                                git \
                                                parallel \
                                                tesseract-ocr \
                                                tesseract-ocr-ces \
                                                tesseract-ocr-deu \
                                                tesseract-ocr-lat \
                                                tesseract-ocr-pol \
                                                tesseract-ocr-fra \
                                                tesseract-ocr-eng \
                                                tesseract-ocr-rus \
                                                tesseract-ocr-ita \
                                                tesseract-ocr-slk \
  ; parallel --will-cite \
  ; git clone https://github.com/tesseract-ocr/tessdata.git /tessdata \
  ; git clone https://github.com/tesseract-ocr/tessconfigs.git /tessdata/tessconfigs